var express = require('express');
var path = require('path');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  const data = require('./mini_log_quotes.csv');

  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Content-disposition', 'attachment; filename=mini_log.csv');
  res.setHeader('Content-Type', 'text/csv');

  res.sendFile(path.join(__dirname, 'mini_log_quotes.csv'));
});

module.exports = router;
